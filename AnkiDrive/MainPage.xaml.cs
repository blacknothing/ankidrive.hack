﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace AnkiDrive
{	
	public partial class MainPage : ContentPage
	{	
		public MainPage ()
		{
			InitializeComponent ();
            BindingContext = new MainViewModel();
		}
	}
}

