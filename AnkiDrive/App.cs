﻿using System;
using Xamarin.Forms;

namespace AnkiDrive
{
    public class App
    {
        public static Page GetMainPage()
        {	
            return new MainPage();
        }
    }
}

