﻿using System;

namespace AnkiDrive
{
    public interface IBluetoothHelper
    {
        void Connect();

        void LightsOn();
        void LightsOff();

        void SetSpeed(int speed, int acceleration);
    }
}

