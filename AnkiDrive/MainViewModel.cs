﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace AnkiDrive
{
    public class MainViewModel : INotifyPropertyChanged
    {
        IBluetoothHelper _bluetooth;

        public event PropertyChangedEventHandler PropertyChanged;

        public MainViewModel()
        {
            _bluetooth = DependencyService.Get<IBluetoothHelper>();
        }
            
        public ICommand ConnectCommand
        {
            get { return new Command(() => _bluetooth.Connect()); }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, 
                    new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}