﻿using System;
using System.Collections.Generic;
using Robotics.Mobile.Core.Bluetooth.LE;
using AnkiDrive.iOS;

[assembly: Xamarin.Forms.Dependency (typeof (BluetoothHelperiOS))]
namespace AnkiDrive.iOS
{
    public class BluetoothHelperiOS : IBluetoothHelper
    {
        public static string ANKI_CAR_SERVICE_UUID = "be15bee1-6186-407e-8381-0bd89c4d8df4";
        private static string UUID_ANKIO_WRITE = "BE15BEE1-6186-407E-8381-0BD89C4D8DF4";
        private static string UUID_ANKIO_READ = "BE15BEE0-6186-407E-8381-0BD89C4D8DF4";
        private static byte[] CMD_PING = new byte[]{0x1, 0x16};
        private static byte[] CMD_VERSION_REQ = new byte[]{0x1, 0x18};
        private static byte[] CMD_BATTERY_LEVEL_REQ = new byte[]{0x1, 0x1a};
        private static byte[] CMD_SET_LIGHTS = new byte[]{0x2, 0x1d, 0};
        private static byte[] CMD_SET_SDK_MODE = new byte[]{0x2, 0x90, 0x01};
        private static byte[] CMD_SET_SPEED = new byte[]{0x6, 0x24, 0, 0, 0, 0, 0};
        private static byte[] CMD_CHANGE_LINE = new byte[]{0x9, 0x25, 0, 0, 0, 0, 0, 0, 0, 0};
        private static byte[] CMD_SET_OFFSET = new byte[]{0x5, 0x2c, 0, 0, 0, 0};

        public static byte MASK_LIGHT_TYPE_FRONTLIGHTS = 4;
        public static byte MASK_LIGHT_TYPE_ENGINE = 8;

        ICharacteristic _characteristic;

        public BluetoothHelperiOS()
        {
        }

        public void Connect()
        {
            Console.Write("Starting Scan");
            Adapter.Current.DeviceDiscovered += OnDeviceDiscovered;
            Adapter.Current.DeviceConnected += OnDeviceConnected;
            Adapter.Current.StartScanningForDevices();
        }

        private void OnDeviceDiscovered (object sender, DeviceDiscoveredEventArgs e)
        {
            if (e.Device.Name != null && e.Device.Name.Contains("Drive"))
            {
                Console.Write("Device Discovered - " + e.Device.Name);
                Adapter.Current.StopScanningForDevices();
                Adapter.Current.ConnectAsync(e.Device);
            }
        }

        private void OnDeviceConnected (object sender, DeviceConnectionEventArgs e)
        {
            Console.WriteLine("Device Connected");
            e.Device.ServicesDiscovered += OnServicesDiscovered;
            e.Device.DiscoverServices();
        }

        private void OnServicesDiscovered (object sender, EventArgs e)
        {
            var device = sender as Device;
            foreach (var s in device.Services)
            {
                Console.WriteLine("ServicesDiscovered + ", s.Name);
                s.CharacteristicsDiscovered += OnCharacteristicsDiscovered;
                s.DiscoverCharacteristics();
            }
        }

        private void OnCharacteristicsDiscovered (object sender, EventArgs e)
        {
            Console.WriteLine("Characteristics Discovered");
            var service = sender as Service;
            foreach (var c in service.Characteristics)
            {
                if (c.Uuid == ANKI_CAR_SERVICE_UUID)
                {
                    _characteristic = c;
                    _characteristic.Write(CMD_SET_SDK_MODE);
                    _characteristic.Write(CMD_VERSION_REQ);
                    _characteristic.Write(CMD_BATTERY_LEVEL_REQ);
                    _characteristic.Write(CMD_SET_OFFSET);
                }
            }
        }

        public void LightsOn()
        {
            CMD_SET_LIGHTS[2] = (byte) (0xf0 | MASK_LIGHT_TYPE_ENGINE | MASK_LIGHT_TYPE_FRONTLIGHTS);
            _characteristic.Write(CMD_SET_LIGHTS);
        }

        public void LightsOff()
        {
            CMD_SET_LIGHTS[2] = (byte) (0x00 | MASK_LIGHT_TYPE_ENGINE | MASK_LIGHT_TYPE_FRONTLIGHTS);
            _characteristic.Write(CMD_SET_LIGHTS);
        }

        public void SetSpeed(int speed, int acceleration)
        {
            CMD_SET_SPEED[2] = (byte)(speed & 0xff);
            CMD_SET_SPEED[3] = (byte)((speed >> 0x08) & 0xff);
            CMD_SET_SPEED[4] = (byte)((acceleration >> 0x08) & 0xff);
            CMD_SET_SPEED[5] = (byte)(acceleration & 0xff);
            _characteristic.Write(CMD_SET_SPEED);
        }
    }
}

