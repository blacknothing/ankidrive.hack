# Flagship Racing

This repository contains a partial C# implementation of the Anki Drive SDK for iOS & the starting point for a Xamarin Forms UI.

The goal is simple - create an app to connect to your Bluetooth car & make it drive around the track.

- https://github.com/anki/drive-sdk
- https://anki.com/en

If that's too easy - extend the app to work on Android and/or Windows Phone... you will need to provide your own platform specific implementation of the IBluetoothHelper.
